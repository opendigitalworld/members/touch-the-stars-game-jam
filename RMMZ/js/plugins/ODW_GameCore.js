//=============================================================================
// Open Digital World - Game Core Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Plugin with specific functions for the game.
 * @author Open Digital World
 * @url https://opendigitalworld.ch
 *
 * @help
 *------------------------------------------------------------------------------
 * Open Digital World - Game Core Plugin
 *------------------------------------------------------------------------------
 *
 * This plugin has been specifically designed for the current game and aims to
 * provide useful functions that can be called via "Script" commands.
 *
 * Term of use: CC0 1.0 Universal
 *
 *------------------------------------------------------------------------------
 * List of functions
 *------------------------------------------------------------------------------ 
 *
 * Damage formulas:
 *   - ODWGameCore.dmgPhysicalBasic( int skillPower, a, b )
 *   - ODWGameCore.dmgMagicalBasic( int skillPower, a, b )
 *
 */

var Imported = Imported || {};
Imported.ODW_GameCore = true;

var ODWGameCore = ODWGameCore || {};
ODWGameCore.pluginName = "ODW_GameCore";

/*
 *------------------------------------------------------------------------------
 * RMMZ Core Overwrites
 *------------------------------------------------------------------------------
 */

// REWRINTING: Hide Gold Windows (height = 0)
Scene_Menu.prototype.goldWindowRect = function() {
    const ww = this.mainCommandWidth();
    const wh = 0; //this.calcWindowHeight(1, true);
    const wx = this.isRightInputMode() ? Graphics.boxWidth - ww : 0;
    const wy = this.mainAreaBottom() - wh;
    return new Rectangle(wx, wy, ww, wh);
};

// REWRINTING: Don't move the battle status window.
Scene_Battle.prototype.updateStatusWindowVisibility = function() {
    if ( $gameMessage.isBusy() ) {
        this._statusWindow.close();
    } else if ( this.shouldOpenStatusWindow() ) {
        this._statusWindow.open();
    }
};

// REWRINTING: Redraw the battle status window.
Scene_Battle.prototype.statusWindowRect = function() {
    const ww = Graphics.boxWidth - this.windowCommandWidth();
    const wh = this.windowAreaHeight() + 8;
    const wx = 0;
    const wy = Graphics.boxHeight - wh + 4;
    return new Rectangle( wx, wy, ww, wh );
};

// REWRINTING: Redraw the battle commands window.
Scene_Battle.prototype.actorCommandWindowRect = function() {
    const ww = this.windowCommandWidth();
    const wh = this.windowAreaHeight();
    const wx = Graphics.boxWidth - ww;
    const wy = Graphics.boxHeight - wh;
    return new Rectangle ( wx, wy, ww, wh );
};

// NEW: Set the actor battle commands window width.
Scene_Battle.prototype.windowCommandWidth = function() {
    return 240; // 192
};

// REWRINTING: Increase the gauge line height.
Window_StatusBase.prototype.gaugeLineHeight = function() {
    return 32; // 24
};

// REWRINTING: Reduce the number of columns based on active party members.
Window_BattleStatus.prototype.maxCols = function() {
    return 1; // 4
};

// NEW: Set the extra width.
Window_BattleStatus.prototype.extraWidth = function() {
    return 10;
};

// REWRINTING: Redraw the actor face and add the actor name.
Window_BattleStatus.prototype.drawItemImage = function( index ) {
    const actor = this.actor( index );
    const rect = this.faceRect( index );
    const nameX = rect.x + this.extraWidth();
    const nameY = rect.y + rect.height + this.extraHeight();
    this.drawActorFace( actor, rect.x, rect.y, rect.width, rect.height );
    this.placeActorName( actor, nameX, nameY );
};

// REWRINTING: Redraw the actor face image.
Window_BattleStatus.prototype.faceRect = function( index ) {
    const rect = this.itemRect( index );
    rect.width = ImageManager.faceWidth; // Use full image width
    rect.height = ImageManager.faceHeight; // Use full image height
	rect.x = rect.x + this.extraWidth(); // Reposition the image
    rect.y = rect.y + 2; // Reposition the image
    return rect;
};

// REWRINTING: Redraw the actor gauges without actor name.
Window_BattleStatus.prototype.drawItemStatus = function( index ) {
    const actor = this.actor( index );
    const rect = this.itemRectWithPadding(index);
    const timeGaugeX = this.timeGaugeX( rect ); // New
    const timeGaugeY = this.timeGaugeY( rect ); // New
    const stateIconX = this.stateIconX( rect );
    const stateIconY = this.stateIconY( rect );
    const basicGaugesX = this.basicGaugesX( rect );
    const basicGaugesY = this.basicGaugesY( rect );
    this.placeTimeGauge( actor, timeGaugeX, timeGaugeY ); // New
    this.placeStateIcon( actor, stateIconX, stateIconY );
    this.placeBasicGauges( actor, basicGaugesX, basicGaugesY );
};

// NEW: Reposition the time gauge in x.
Window_BattleStatus.prototype.timeGaugeX = function( rect ) {
    return rect.x + ImageManager.faceWidth + this.extraWidth();
};

// NEW: Reposition the time gauge in y.
Window_BattleStatus.prototype.timeGaugeY = function( rect ) {
    return this.basicGaugesY( rect ) - this.gaugeLineHeight();
};

// REWRINTING: Reposition the basic gauges in x.
Window_BattleStatus.prototype.basicGaugesX = function( rect ) {
    return rect.x + ImageManager.faceWidth + this.extraWidth(); // Right to the actor face image
};

// REWRINTING: Reposition the basic gauges in y.
Window_BattleStatus.prototype.basicGaugesY = function(rect) {
    const bottom = rect.y + rect.height - 6; // Reposition the gauges
    const numGauges = $dataSystem.optDisplayTp ? 3 : 2;
    return bottom - this.gaugeLineHeight() * numGauges;
};

// REWRINTING: Reposition the state icons in x.
Window_BattleStatus.prototype.stateIconX = function(rect) {
    return rect.x + this.extraWidth(); // Aligned to right.
};

// REWRINTING: Reposition the state icons in y.
Window_BattleStatus.prototype.stateIconY = function(rect) {
    return rect.y + ImageManager.iconHeight / 2 + 2; // 4
};

// REWRINTING: Increase the gauge bitmap width.
Sprite_Gauge.prototype.bitmapWidth = function() {
    return 532; // 128
};

// REWRINTING: Increase the gauge bitmap height.
Sprite_Gauge.prototype.bitmapHeight = function() {
    return 32; // 24
};

// REWRINTING: Increase the gauge height.
Sprite_Gauge.prototype.gaugeHeight = function() {
    return 20; // 12
};

// REWRINTING: Fix the gauge position in x.
Sprite_Gauge.prototype.gaugeX = function() {
    return 160; // 0 or dependig ot the label width
};

// REWRINTING: Reduce the gauge label position in y.
Sprite_Gauge.prototype.labelY = function() {
    return 3; // 3
};

// REWRINTING: Add time gauge label.
Sprite_Gauge.prototype.label = function() {
    switch (this._statusType) {
        case "time":
            return 'Next action'; // New
        case "hp":
            return TextManager.hp;
        case "mp":
            return TextManager.mp;
        case "tp":
            return TextManager.tp;
        default:
            return "";
    }
};

// REWRINTING: Redraw gauge with time gauge label.
Sprite_Gauge.prototype.redraw = function() {
    this.bitmap.clear();
    const currentValue = this.currentValue();
    if ( ! isNaN( currentValue ) ) {
        this.drawGauge();
        this.drawLabel(); // Always draw label
        if (this._statusType !== "time" && this.isValid() ) {
            this.drawValue();
        }
    }
};

/*
 *------------------------------------------------------------------------------
 * Damage formulas
 *------------------------------------------------------------------------------
 */

ODWGameCore.dmgBase = function( skillPower, attacker, defender ) {
	
	return skillPower * attacker.atk / defender.def;
	
}

ODWGameCore.dmgAdvAtk = function( skillPower, attacker, defender ) {
	
	let defFactor = defender.hp / defender.mhp;
	
	return skillPower * attacker.atk / ( defender.def * defFactor );
	
}

ODWGameCore.dmgAdvDef = function( skillPower, attacker, defender ) {
	
	let atkFactor = attacker.hp / attacker.mhp;
	
	return skillPower * ( attacker.atk * atkFactor ) / defender.def;
	
}
