# Storyboard

## Title - Picture

...

## Panel 1 - Picture

Teddy est assis autour de divers jouets.

Teddy: Quels temps merveilleux nous avons vécu! Remplis de joie, de jeux.

## Panel 2 - Picture

Teddy est assis au pied du lit, dans la pénombre.

Teddy:

## Panel 3 - Picture

Teddy est assis dans une boîte avec d'autres jouets cassés.

Teddy:

## Panel 4 - Black Screen

Voice: Teddy! (wait) Teddy! (wait) Réveille-toi!

Teddy: Quoi? (wait) Cette voix!

Voice: Teddy, mon cher, lève-toi!

Teddy: Est-ce bien toi? (wait) Où es-tu?

Voice: Teddy, je suis là. (wait) Rejoins-moi, vite, je t'en prie! Ted....

Teddy: Attends! Je... J'arrive...

## Map01 - Dump

- **Mission:** sortir de la décharge
- **Ennemis:** rats, mouches, guêpes
- **Armes:** crayon, briquet -> à chercher
- **Armures:** bouteille de PET -> à chercher
- **Objets:** recharge de gaz -> à chercher
- **Objets clés:** adresse de la maison d'enfance

Teddy: Une décharge! Je suis au mileur

Teddy est face à une croisée de chemin.

Teddy: Par où passer maintenant? La route est le chemin le plus direct, mais le trafic est important. Ou alors la forêt, avec ses bêtes sauvages?

## Map02a - Road

- **Mission:** aller en ville
- **Ennemis:** voitures
- **Armes:** N/A
- **Armures:** N/A
- **Objets:** N/A
- **Objets clés:** N/A

Teddy: La ville se trouve au bout de cette route.

_Son de circulation, voiture qui passe._

Teddy: Bon, je vais devoir rester prudent pour ne pas qu'une voiture me heurte.

## Map02b - Forest

- **Mission:** aller en ville
- **Ennemis:** rats, renards, hiboux, mouches, guêpes
- **Armes:** N/A
- **Armures:** N/A
- **Objets:** N/A
- **Objets clés:** N/A

Teddy: La ville se trouve de l'autre côté de cette forêt.

_Son d'animaux de la forêt_

Teddy: Bon, je vais devoir rester prudent pour ne pas me faire remarquer.

## Map03 - City

- **Mission:** aller dans le quartier résidentiel
- **Ennemis:** rats, chats, chiens, citadins, voitures
- **Armes:** tournevis, poêle, spray coiffant -> à chercher dans les poubelles
- **Armures:** brique de lait, bol -> à chercher dans les poubelles
- **Objets:** recharge de gaz -> à chercher dans les poubelles
- **Objets clés:** N/A

Teddy: 

## Map04 - Residential area

- **Mission:** trouver la maison d'enfance
- **Ennemis:** chats, chiens, enfants
- **Armes:** N/A
- **Armures:** N/A
- **Objets clés:** Fleur de chrysanthème.

## Map05 - Graveyard

- **Mission:** aller vers la tombe de l'enfant et combattre le fantôme
- **Ennemis:** aucun
- **Armes:** N/A
- **Armures:** N/A
- **Objets clés:** N/A

_

Ghost: 

_

## Panel 5 - Ending

Teddy et l'enfant volent vers les étoiles. + Message "FIN"

## Panel 6 - Credits

Affichage des crédits et des remerciements.

