# Touch The Stars Game Jam

RPG developed specifically for "Touch The Stars Game Jam" from Komodo / Studio Blue.

## Assets Used

### Icons
- Material Icons, by Google

### Paralax
- Default RPG Maker MZ

### Battlebacks

- Default RPG Maker MV
- MV Trinity Resource Pack

### Character Generator Parts

- Default RPG Maker MZ
- MV Trinity Resource Pack

### Characters & Battlers

- Creatures: Woodland Animals
- Hidenone Resource
- Avery Resource
- Rondine Resource
- AlephNull Resource

### Tilesets

- RPG Maker Default MV
- FSM : Autumn Woods and Rural Tiles
- Modern + Outer Basic

### Plugins

- VisuStella MZ

### BGM

- Emotional II: Voices of Angels
	- Title: A World We Once Dreamed Of
	- Beginning: Distant Memories
	- Intermission 1: Follow Your Dreams
	- Intermission 2: Remembrance and Heartache
	- Intermission 3: Rest in Peace
	- Graveyard 1: Overwhelmed with Despair
	- Graveyard 2: The Place Where Angels Go
	- Ending: Carry Me Home on Your Wings
	- Game Over: There Were Better Times

- ​Eternal Melodies
	- Battle Map: To Arms
	- Battle Boss: Revelations
	- Dump: Journey Onward
	- Forest: An Iron Heart
	- Highway: Brave Adventure
	- Village: Premonition
